#ifndef GAME_H
#define GAME_H

#include "maps.h"

#define RCL_PIXEL_FUNCTION pixelFunc 

#include "raycastlib.h"

typedef uint8_t Key;

#define KEY_UP     0
#define KEY_RIGHT  1
#define KEY_DOWN   2
#define KEY_LEFT   3
#define KEY_SHOOT  4
#define KEY_JUMP   5
#define KEY_CANCEL 6
#define KEY_USE    7
#define KEY_QUIT   8
#define NUM_KEYS   9

/// general 256 color palette, 565 values

const uint16_t palette[256] =
{
  0, 4226, 8452, 12678, 16936, 21162, 25388, 29614, 35921, 40147, 44373, 48599,
  52857, 57083, 61309, 65535, 4193, 8418, 12611, 16836, 21029, 25254, 29447,
  33672, 37865, 42090, 46283, 50508, 54701, 58926, 63119, 65264, 2177, 6402,
  8579, 12804, 14981, 19206, 21383, 25608, 27785, 32010, 34187, 38412, 40589,
  44814, 46991, 51184, 2177, 4354, 6531, 8709, 10886, 13063, 15240, 17418,
  19595, 21772, 23949, 26127, 28304, 30481, 32658, 34804, 2178, 4356, 6534,
  8712, 10890, 13068, 15246, 17424, 19602, 21780, 23958, 26136, 28314, 30492,
  32670, 34815, 2114, 4260, 6374, 8520, 10634, 12780, 14894, 17040, 19154,
  21300, 23414, 25560, 27674, 29820, 31934, 34079, 2114, 6276, 8390, 12552,
  14666, 18828, 20942, 25104, 29266, 31380, 35542, 37656, 41818, 43932, 48094,
  50207, 4161, 8323, 12485, 16646, 20808, 24970, 29131, 33293, 37455, 41616,
  45778, 49940, 54101, 58263, 62425, 64538, 4096, 8192, 12288, 16384, 20480,
  24576, 28672, 32768, 36864, 40960, 45056, 49152, 53248, 57344, 61440, 63488,
  4192, 8384, 12576, 16768, 20960, 25152, 29344, 33536, 37728, 41920, 46112,
  50304, 54496, 58688, 62880, 64992, 2176, 4352, 6528, 8704, 10880, 13056,
  15232, 17408, 19584, 21760, 23936, 26112, 28288, 30464, 32640, 34784, 128,
  257, 385, 514, 642, 771, 899, 1028, 1156, 1285, 1413, 1542, 1670, 1799, 1927,
  2024, 130, 260, 390, 520, 650, 780, 910, 1040, 1170, 1300, 1430, 1560, 1690,
  1820, 1950, 2047, 34, 68, 102, 136, 170, 204, 238, 272, 306, 340, 374, 408,
  442, 476, 510, 543, 2050, 4100, 6150, 8200, 10250, 12300, 14350, 18448,
  20498, 22548, 24598, 26648, 28698, 30748, 32798, 36895, 4097, 8194, 12292,
  16389, 20486, 24584, 28681, 32779, 36876, 40973, 45071, 49168, 53265, 57363,
  61460, 63509
};

//-----------------------------------------------------------------------------

/*
  Interface to the frontend, implement these.
*/

static const int16_t FE_screenWidth;
static const int16_t FE_screenHeight;

void FE_init();                      ///< Init the frontend.
void FE_destroy();                   ///< Init the frontend.
void FE_frameStart();                ///< Called at the beginning of each frame.
void FE_frameEnd();                  ///< Called at the end of each frame.
int  FE_keyIsDown(Key key);          ///< Check if given key is currently pressed.
static inline void FE_putPixel(uint16_t x, uint16_t y, uint8_t index);

//-----------------------------------------------------------------------------

RCL_RayConstraints constraints;
RCL_Camera camera;

#define HUE(c) (c * 16 + 8)    ///< Gives a middle color of given hue (0 to 15).

static inline uint8_t addIntensity(uint8_t color, int16_t intensity)
{
  int16_t newValue = (color & 0b00001111) + intensity; // value as in HSV

  if (newValue <= 0)
    return 0; // black

  if (newValue >= 16)
    newValue = 15;

  return (color & 0b11110000) | newValue;
}

void pixelFunc(RCL_PixelInfo *pixel)
{
  char color = pixel->isFloor ? HUE(3) : HUE(5);

  color = addIntensity(color,-1 * pixel->depth / RCL_UNITS_PER_SQUARE);

  FE_putPixel(pixel->position.x,pixel->position.y, color);
}

RCL_Unit getSquareFloorHeight(int16_t x, int16_t y)
{
  return (x < 0 || x > 20 || y < 0 || y > 20) ? RCL_UNITS_PER_SQUARE * 2 : 0;
}

RCL_Unit getSquareCeilingHeight()
{
  return RCL_UNITS_PER_SQUARE * 20;
}

RCL_Unit getSquareType()
{
  return 0;
}

void run()
{
  RCL_initRayConstraints(&constraints);
  RCL_initCamera(&camera);

  camera.position.x = 500;
  camera.position.y = 500;

  camera.resolution.x = FE_screenWidth;
  camera.resolution.y = FE_screenHeight;

  FE_init();

  while (1)
  {
    FE_frameStart();

    if (FE_keyIsDown(KEY_QUIT))
      break;
    
    RCL_Vector2D direction = RCL_angleToDirection(camera.direction);

    direction.x /= 10;
    direction.y /= 10;

    int move = 0;

    if (FE_keyIsDown(KEY_UP))
    {
      move = 1;
    }
    else if (FE_keyIsDown(KEY_DOWN))
    {
      direction.x *= -1;
      direction.y *= -1;
      move = 1;
    }

    if (move)
      RCL_moveCameraWithCollision(&camera, direction, 0, getSquareFloorHeight,
      getSquareCeilingHeight, 1, 0);

    if (FE_keyIsDown(KEY_RIGHT))
      camera.direction += 1;
    else if (FE_keyIsDown(KEY_LEFT))
      camera.direction -= 1;

    RCL_renderComplex(camera, getSquareFloorHeight, getSquareCeilingHeight,
      getSquareType, constraints);

    FE_frameEnd();
  }
}

#endif
