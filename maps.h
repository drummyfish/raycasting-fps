#ifndef MAPS_H
#define MAPS_H

#include "stdint.h"

#define MAP_WIDTH 32
#define MAP_HEIGHT 32

typedef uint16_t MapTile;

/**
  5 bits: floor height level
  5 bits: ceiling height level
  5 bits: texture
  1 bits: is special
*/

#define T(type, height, texture)

typedef struct
{
  uint8_t tiles[MAP_WIDTH * MAP_HEIGHT];
} Map;

#endif
