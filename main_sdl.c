#include <stdio.h>
#include <stdint.h>
#include <SDL2/SDL.h>

#include "game.h"

uint16_t *pixels;
Key keyStates[NUM_KEYS];

SDL_Window *window;
SDL_Renderer *renderer;
SDL_Texture *texture;
SDL_Surface *screenSurface;

uint8_t keyFromSDL(int scancode)
{
  switch (scancode)
  {
    case SDL_SCANCODE_UP:     return KEY_UP;    break;
    case SDL_SCANCODE_RIGHT:  return KEY_RIGHT; break;
    case SDL_SCANCODE_DOWN:   return KEY_DOWN;  break;
    case SDL_SCANCODE_LEFT:   return KEY_LEFT;  break;
    default:                  return 255;       break;
  }
}

int main()
{
  run();
  return 0;
}

//-----------------------------------------------------------------------------
// interface implementation

static const int16_t FE_screenWidth = 640;
static const int16_t FE_screenHeight = 480;

void FE_putPixel(uint16_t x, uint16_t y, uint8_t index)
{
  pixels[y * FE_screenWidth + x] = palette[index];
}

void FE_init()
{
  for (uint8_t i = 0; i < NUM_KEYS; ++i)
    keyStates[i] = 0;

  pixels = malloc(FE_screenWidth * FE_screenHeight * sizeof(int16_t));
  window = SDL_CreateWindow("game", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, FE_screenWidth, FE_screenHeight, SDL_WINDOW_SHOWN); 
  renderer = SDL_CreateRenderer(window,-1,0);
  texture = SDL_CreateTexture(renderer,SDL_PIXELFORMAT_RGB565, SDL_TEXTUREACCESS_STATIC, FE_screenWidth, FE_screenHeight);
  screenSurface = SDL_GetWindowSurface(window);
}

void FE_destroy()
{
  free(pixels);

  // TODO: destroy SDL
}

void FE_frameStart()
{
  SDL_Event event;

  while (SDL_PollEvent(&event))
  {
    switch (event.type)
    {
      case SDL_QUIT:
        keyStates[KEY_QUIT] = 1;
        break;

      case SDL_KEYDOWN:
      {
        uint8_t key = keyFromSDL(event.key.keysym.scancode);
     
        if (key != 255)
          keyStates[key] = 1; 

        break;
      }

      case SDL_KEYUP:
      {
        uint8_t key = keyFromSDL(event.key.keysym.scancode);
     
        if (key != 255)
          keyStates[key] = 0; 

        break;
      }

      default:
        break;
    }
  }
}

void FE_frameEnd()
{
  SDL_UpdateTexture(texture,NULL,pixels,FE_screenWidth * sizeof(uint16_t));
  SDL_RenderClear(renderer);
  SDL_RenderCopy(renderer,texture,NULL,NULL);
  SDL_RenderPresent(renderer);
}

int FE_keyIsDown(uint8_t key)
{
  return keyStates[key];
}

