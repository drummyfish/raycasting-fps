#!/bin/bash

clear
clear

rm raycastlib.h
cp ../raycastlib/raycastlib.h .

gcc -g -x c -Wall -Wextra -pedantic main_sdl.c -o main -lSDL2 && ./main
